import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  valor: number = 0;

  No(){
    console.log('No se presiono ningun boton');
    
  }
  btn1(){
    this.valor=1;
  }

  btn2(){
    this.valor=2;
  }

  btn3(){
    this.valor=3;
  }

  constructor() { 
    this.No();
  }

  ngOnInit(): void {
  }

}
